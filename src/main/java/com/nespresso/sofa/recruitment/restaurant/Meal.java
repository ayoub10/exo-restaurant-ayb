package com.nespresso.sofa.recruitment.restaurant;

public class Meal {
	
	public static int durationTomatoMozzarellaSalad = 6;
	
	private int numberOfDishes;


	public int servedDishes() {
		return getNumberOfDishes();
	}

	public String cookingDuration() {
		if(servedDishes()>1){
			return (durationTomatoMozzarellaSalad+((servedDishes()-1)*(durationTomatoMozzarellaSalad/2)))+"";
		}
		return durationTomatoMozzarellaSalad*servedDishes()+"";
	}

	public void setNumberOfDishes(int numberOfDishes) {
		this.numberOfDishes = numberOfDishes;
	}

	public int getNumberOfDishes() {
		return numberOfDishes;
	}
	
	
	

}
