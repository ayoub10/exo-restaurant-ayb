package com.nespresso.sofa.recruitment.restaurant;

import java.util.List;

import com.nespresso.sofa.recruitment.restaurant.exceptions.UnavailableDishException;
import com.nespresso.sofa.recruitment.restaurant.util.IngredientExtractor;

public class Restaurant {

	List<? extends ParentType> ingredients;
	IngredientExtractor extractor = new IngredientExtractor();

	public Restaurant(String... string) {

		ingredients = (List<Ingredient>) extractor.extractElements(string);

	}

	public Ticket order(String string) {
		ParentType order = extractor.itemParser(string);
		try {
			if (checkAvailability(order)) {
				return new Ticket(order);
			} else {
				throw new UnavailableDishException("There isn't enough of ingredients");
			}
		} catch (UnavailableDishException e) {
			return null;
		}
	}

	private boolean checkAvailability(ParentType order) {
		int quantity = order.getQuantity();
		int ballOfMozzarella = ingredients.get(0).quantity;
		int tomatoes = ingredients.get(1).quantity;
		
		if (ballOfMozzarella > quantity && (tomatoes / 2) > quantity) {
			return true;
		}
		return false;
	}

	public Meal retrieve(Ticket ticket) {

		int num = ticket.getOrder().getQuantity();
		Meal meal = new Meal();
		meal.setNumberOfDishes(num);

		return meal;
	}

}
