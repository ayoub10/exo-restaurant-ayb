package com.nespresso.sofa.recruitment.restaurant.util;

import java.util.ArrayList;
import java.util.List;

import com.nespresso.sofa.recruitment.restaurant.ParentType;

public class IngredientExtractor {

	
	 public List<? extends ParentType> extractElements(String... string) {
		List<ParentType> items = new ArrayList<ParentType>();

		for (String itemInput : string) {
			ParentType item =  itemParser(itemInput);
			items.add( item);
		}
		return items;
	}

	public ParentType itemParser(String IngredientInput) {
		String[] IngredientProperties = IngredientInput.split(" ");

		//Fix me : Shouldn't return 0 in unlimited quantity case
		int quantity = 0;
		String name = "";
		try {
			quantity = new Integer(IngredientProperties[0]);
		} catch (NumberFormatException e) {
			name += IngredientProperties[0];
		} finally {
			for (int i = 1; i < IngredientProperties.length; i++) {
				name += " " + IngredientProperties[i];
			}
		}
		ParentType item = new ParentType(quantity, name);
		return item;
	}
}
