package com.nespresso.sofa.recruitment.restaurant;

public class ParentType {

	int quantity;
	String name;
	

	public ParentType(int quantity, String name) {
		this.quantity = quantity;
		this.name = name;
	}


	public int getQuantity() {
		return quantity;
	}


	public String getName() {
		return name;
	}
	
	
}
