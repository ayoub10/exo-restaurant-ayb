package com.nespresso.sofa.recruitment.restaurant.exceptions;

public class UnavailableDishException extends Exception {


	public UnavailableDishException(String message) {
		super(message);
	}

}
