package com.nespresso.sofa.recruitment.restaurant;

public class Ticket {

	private ParentType order;

	public ParentType getOrder() {
		return order;
	}

	public Ticket(ParentType order) {
		this.order = order;
	}

}
